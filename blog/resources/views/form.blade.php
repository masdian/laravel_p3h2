<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/daftar" method="POST">
        @csrf

        <label for="first_name">First Name:</label>
        <br><br>
        <input type="text" name="first_name">

        <br><br>

        <label for="">Last Name:</label>
        <br><br>
        <input type="text" name="last_name">
        <br><br>

        <label>Gender:</label>
        <br><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="0">Female <br>
        <input type="radio" name="gender" value="99">Other <br>
        <br>
        <label>Nationality:</label>
        <br><br>
        <select>
            <option>Indonesia</option>
            <option>Malaysia</option>
            <option>Singapore</option>
            <option>Thailand</option>
            <option>Brunei</option>
            <option>Vietnam</option>
            <option>Timor Leste</option>
        </select>
        <br><br>
        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="2">English <br>
        <input type="checkbox" name="language" value="3">Other <br>
        <br>
        <label for="bio">Bio:</label>
        <br><br>
        <textarea cols="25" rows="7" name="bio"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    
    </form>

</body>
</html>