<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class blogController extends Controller
{
    public function index() {
        return view('index');
    }

    public function form() {
        return view('form');
    }

    public function daftar(Request $request) {
        $first = $request -> first_name;
        $last = $request -> last_name;
        return view('welcome', compact('first', 'last'));
    }

    public function welcome() {
        return view('welcome');
    }
}
